# Dockerfile Example

## Prerequisites

Make sure you have Docker installed

## Build the container image

```
docker build -t hello-world-example .
```

The `-t hello-world-example` arguments tell docker to "tag" the image with the name "hello-world-example".

## Create and run a container from the image 

```
docker run --rm -it --name hello-world-container hello-world-example
```

You should see "hello, world" printed to the command line.

Arguments explained:

- `--rm` removes the container after it is finished running
- `-it` runs the container interactively in our current shell
- `--name hello-world-container` names the container "hello-world-container" (you'll see this if you run `docker container ls` while the container is running).

This container will be cleaned up almost as soon as it's created due to the `--rm` flag, so you may not even see the name ever show up.  I've included the "name" tag here to highlight that images!=containers.

## What's next

This is such a minimal example that it's hard to understand the need for Docker really.

A good "next step" is to launch a shell inside of the container and poke around to see what you find.  Your command prompt will change (because you are now the root user *inside* of your container), but it'll look a lot like a very empty linux system.  You can either launch a shell through Docker Desktop, but a better way is to *override the entrypoint* from the command line.

```
docker run --rm -it --entrypoint /bin/sh --name hello-world-container hello-world-example
```

Note: On windows you may need to use

```
docker run --rm -it --entrypoint //bin/sh --name hello-world-container hello-world-example
```

Here, instead of running `./hello_world.py` as soon as the container launches (i.e. the "entrypoint"), we run `/bin/sh`, which gives us a shell prompt.  Because we've run the container interactively with `-it` we can poke around inside of the container.  If you run `ls`, you should see our `hello_world.py` script.  Poke around, try finding stuff you recognize from other linux systems, try adding other software to the container (in alpine linux `apk add` is the equivalent of `apt install`).  The good news is, if you break it, you can just create a new container from the other image and start over!