FROM alpine:latest

RUN apk add python3
COPY hello_world.py /
RUN chmod +x hello_world.py

ENTRYPOINT ["./hello_world.py"]
